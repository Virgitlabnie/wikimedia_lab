echo "delete wikimediachanges topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic wikimediachanges
echo "create wikimediachanges topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --config retention.ms=3600000 --topic wikimediachanges

echo "delete wikipediachanges topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server localhost:9092 --topic wikipediachanges
echo "create wikipediachanges topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic wikipediachanges

#if $KAFKA_HOME/bin/kafka-topics.sh --list --bootstrap-server localhost:9092 | grep -q wikimediachanges; #then
#   echo "delete existing wikimediachanges topic"
#   $KAFKA_HOME/bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic wikimediachanges
#else
#   echo "there was no wikimediachanges topic yet"
#fi
