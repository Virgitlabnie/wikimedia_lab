package file_example;

import java.io.FileWriter;
import java.io.IOException;

public class Charade {

	public static void main(String[] arg) {
		new Charade();
	}

	Charade() {
		String fileName = "/tmp/charade.txt"; // the system-dependent filename
		boolean append = true;// if true, then data will be written to the end of the file rather than the
		// beginning

		FileWriter fileWriter = null;

		try {
			// [create if necessary and] open the file
			fileWriter = new FileWriter(fileName, append);
		} catch (IOException e) {
			System.err.println("Error while creating or opening " + fileName + ": " + e.getMessage());
			System.err.println("Possible cause: " + fileName + " exists but is a directory rather than a regular file, "
					+ fileName + " does not exist but cannot be created, or cannot be opened for any other reason");
			System.exit(1);
		}

		try {
			// write to the file
			fileWriter.write("My first is often at the front door.\n");
			fileWriter.write("My second is found in the cereal family.\n");
			fileWriter.write("My third separates rich from poor.\n");
			fileWriter.write("My whole is one of the united states.\n");
		} catch (IOException e) {
			System.err.println("Error while writing to " + fileName + ": " + e.getMessage());
			System.exit(1);
		}

		try {
			// close
			fileWriter.close();
		} catch (IOException e) {
			System.err.println("Error while closing " + fileName + ": " + e.getMessage());
			System.exit(1);
		}
	}
}
